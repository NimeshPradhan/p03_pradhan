//
//  Brick.h
//  p03-pradhan
//
//  Created by Nimesh on 2/17/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Brick : UIImageView

@property (nonatomic) Boolean gameOver;
@property (nonatomic) Boolean isScored;
@property (nonatomic) float dx, dy;
@property (nonatomic) int reverse;
@property (nonatomic) int brickSpeed;

@end
