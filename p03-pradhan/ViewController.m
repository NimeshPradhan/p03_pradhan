//
//  ViewController.m
//  p03-pradhan
//
//  Created by Nimesh on 2/17/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation ViewController
@synthesize  gameOverAlert;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor blackColor];
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)speedChange:(id)sender {

    UISlider *s = (UISlider *)sender;
 //   NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s value]];}

@end
