//
//  LaunchViewController.h
//  p03-pradhan
//
//  Created by Nimesh on 2/25/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchViewController : UIViewController


@property (nonatomic,strong) IBOutlet UIImageView *background;
@end
