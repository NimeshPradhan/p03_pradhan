//
//  GameView.m
//  p03-pradhan
//
//  Created by Nimesh on 2/17/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize  jumper,bricks;
@synthesize tilt;
@synthesize  background;
@synthesize label, stage;
@synthesize leftEye,rightEye;

int bricksScored=0;
int movingBricks=1;
int randomBrick[10];
int level = 1;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, super.bounds.size.width,super.bounds.size.height) ];
        [background setImage:[UIImage imageNamed:@"Aku-2.jpg"]];
        [self addSubview:background];
        [self sendSubviewToBack:background];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(260, 15, 100, 70)];
        stage = [[UILabel alloc] initWithFrame:CGRectMake(130, 15, 100, 70)];
        [self initLabels];
        
        jumper = [[Jumper alloc] initWithImage:[UIImage imageNamed: @"samurai_left.png"]];
        [jumper setDx:0];
        [jumper setDy:9];
        [self addSubview:jumper];
        
        [self initEye];
        
        [self makeBricks];
        
        [NSTimer scheduledTimerWithTimeInterval:3.0f
                 target:self
                 selector:@selector(changeBrick)
                 userInfo:nil
                 repeats:YES];
        
        
        
    }
    return self;
}

-(void)initEye{
    
    leftEye = [[UILabel alloc] initWithFrame:CGRectMake(188, 131, 5, 5)];
    rightEye = [[UILabel alloc] initWithFrame:CGRectMake(208, 131, 5, 5)];
    leftEye.layer.opaque = true;
    leftEye.backgroundColor = [UIColor blackColor];
    rightEye.backgroundColor = [UIColor blackColor];
    leftEye.layer.masksToBounds = true;
    rightEye.layer.masksToBounds = true;
    rightEye.layer.opaque = true;
    leftEye.layer.cornerRadius = 2.5f;
    rightEye.layer.cornerRadius = 2.5f;
    [self addSubview:leftEye];
    [self addSubview:rightEye];

}

-(void)initLabels{

    NSString *score =[NSString stringWithFormat:@"Score: \n%d", bricksScored];
    label.text = score;
    label.opaque=true;
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor=[UIColor redColor];
    label.layer.masksToBounds = true;
    label.font = [UIFont boldSystemFontOfSize:25.0];
    label.layer.cornerRadius = 10.0;
    label.numberOfLines = 2;
    [self addSubview:label];
    
    score =[NSString stringWithFormat:@"Level: \n%d", level];
    stage.text = score;
    stage.opaque=true;
    stage.textAlignment = NSTextAlignmentCenter;
    stage.backgroundColor=[UIColor redColor];
    stage.layer.masksToBounds = true;
    stage.font = [UIFont boldSystemFontOfSize:25.0];
    stage.layer.cornerRadius = 10.0;
    stage.numberOfLines = 2;
    [self addSubview:stage];

}


-(void)setRandomBrick{
    int lst = 0;
    while(lst<10){
        Boolean present = false;
        int random = arc4random_uniform(10);
        for(int i = 0; i < lst; i++){
            if(randomBrick[i]==random){
                present = true;
                break;
            }
        }
        if(!present){
        randomBrick[lst]=random;
        lst++;
        }
    }
}


-(void)makeBricks
{
    
    CGRect bounds = [self bounds];
    [self setRandomBrick];
   // NSLog(@"random called and finished");
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 10; ++i)
    {
        Brick * b = [[Brick alloc] initWithImage:[UIImage imageNamed: @"green_brick.png"]];
        [b setGameOver:false];
        [b setIsScored:false];
        [b setDx:0];
        [b setDy:0];
        [b setReverse:arc4random_uniform(2)];
        [b setBrickSpeed:arc4random()%4+1];//assign random speed to brick
        [self addSubview:b];
        [b setCenter:CGPointMake(rand() % (int)(bounds.size.width*0.8+28-1)+28 , (int)(bounds.size.height)/(11)* i+30)];
        [bricks addObject:b];
    }
    [self changeBrick];
}

-(void)changeBrick{
    
    for(Brick *brick in bricks){
        if(brick.gameOver==true){
            if(brick.isScored==false)
                [brick setImage:[UIImage imageNamed: @"green_brick.png"]];
            else
                [brick setImage:[UIImage imageNamed: @"yellow_brick.png"]];
            [brick setGameOver:false];
        }
    }
    int index = arc4random()%9+1;
    Brick *brick = [bricks objectAtIndex:index];
    [[bricks objectAtIndex:index] setImage:[UIImage imageNamed: @"red_brick.png"]];
    brick.gameOver=true;

}

-(void)arrange:(CADisplayLink *)sender
{
   // CFTimeInterval ts = [sender timestamp];
  //  NSLog(@"tilt:%f", (float)tilt);
    CGRect bounds = [self bounds];
    
    CGPoint ly = [leftEye center];
    CGPoint ry = [rightEye center];
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    //move Aku's eyes vertical if jumper goes higher than eyes
    if(p.y<ly.y&&ly.y>129){
        ly.y-=0.2;
        ry.y-=0.2;
    }
    if(p.y>ly.y && ly.y<=131){
        ly.y=ry.y=131;
    }
    
    
    //jumpers sees in the direction of tilt
    //Aku's eyes moves in direction of jumper
    if(tilt>0){
        if(ly.x < 195 )
            ly.x += 0.1;
        if(ry.x < 215)
            ry.x += 0.1;
        [jumper setImage:[UIImage imageNamed: @"samurai_right.png"]];
    }
    if(tilt<0){
        
        if(ly.x > 184 )
            ly.x -= 0.1;
        if(ry.x > 204)
            ry.x -= 0.1;
        [jumper setImage:[UIImage imageNamed: @"samurai_left.png"]];
    }


    
    // horizontal brick movement
    // move multiple bricks at at time
    for(int i=0;i<movingBricks;i++){
        //NSLog(@"i:%d",i);
        CGPoint bp = [[bricks objectAtIndex:randomBrick[i]] center];
        int reverse = [[bricks objectAtIndex:randomBrick[i]] reverse];
        int speed =  [[bricks objectAtIndex:randomBrick[i]] brickSpeed];
  
        if(reverse == 0)
            bp.x -= speed;
        else
            bp.x +=speed;
        //reverse the direction when brick reaches the corner
        if(bp.x < 0+28){
            [[bricks objectAtIndex:randomBrick[i]] setReverse:1];
            bp.x += abs(speed);
        }
        if(bp.x > bounds.size.width-28){
            [[bricks objectAtIndex:randomBrick[i]] setReverse:0];
            bp.x -= abs(speed);
        }
        [[bricks objectAtIndex:randomBrick[i]] setCenter:bp];
    }
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height-50)
    {
        [jumper setDy:9];
        p.y = bounds.size.height-50;
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0)
        p.y += bounds.size.height;
    
    // If we have gone too far left, or too far right, be there until tilted
    if (p.x < 20)
        p.x = 20;
    if (p.x > bounds.size.width-20)
        p.x = bounds.size.width-20;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    Boolean newScore;
    
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            newScore = true;
            if(brick.isScored == true)
                newScore = false;
            p.y +=25;
            if(CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                //wait! is this red brick? Ooops, wrong brick
                if(brick.gameOver==true){
                  //  NSLog(@"Game Over");
                    for (Brick *brick in bricks)
                    {
                        [brick removeFromSuperview];
                    }
                    [leftEye removeFromSuperview];
                    [rightEye removeFromSuperview];
                    [label removeFromSuperview];
                    [stage removeFromSuperview];
                    [jumper removeFromSuperview];

                    [background setImage:[UIImage imageNamed:@"Jack.jpg"]];
                    
                    NSString *alertMessage = [NSString stringWithFormat:@"Oh no! You loose\nYour Score:\n%d",bricksScored];
                    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:alertMessage message: @"Kill again?" preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"Yes"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [self resetGame];
                                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
    
                    [myAlertController addAction: ok];
                    UIViewController *currentTopVC = [self currentTopViewController];
                    [currentTopVC presentViewController:myAlertController animated:true completion:nil];
                }
                
             //   NSLog(@"Bounce!");
                [brick setImage:[UIImage imageNamed: @"yellow_brick.png"]];
                [brick setIsScored:true];
                [jumper setDy:9];
            //if brick is not already scored, increment total score
                if(newScore&&!brick.gameOver){
                    bricksScored++;
                    //update score
                    NSString *score =[NSString stringWithFormat:@"Score: \n%d", bricksScored];
                    label.text = score;
                }
                
                //NSLog(@"brick scored:%d",bricksScored);
                //if all bricks are scored then replace them with new bricks
                if(bricksScored> 0 && bricksScored%10==0){
                    [self makeBricks];
                
                    //increase number of moving bricks
                    movingBricks++;
                
                    //cap the number of moving tiles to 10
                    if(movingBricks>=10)
                        movingBricks=10;
                    level++;
                    NSString *level_str =[NSString stringWithFormat:@"Level: \n%d", level];
                    stage.text = level_str;
                    
                }
            }
            p.y-=25;
        }
        
    }
    
    [jumper setCenter:p];
    [leftEye setCenter:ly ];
    [rightEye setCenter:ry];    // NSLog(@"Timestamp %f", ts);
}

-(void)resetGame{
    bricksScored = 0;
    movingBricks = 1;
    level = 1;
    label.text = [NSString stringWithFormat:@"Score:\n%d",bricksScored];
    stage.text = [NSString stringWithFormat:@"Level: \n%d", level];
    
    [self addSubview:label];
    [self addSubview:stage];
    [self makeBricks];
    [self addSubview:jumper];
    
    [self initEye];
    [background setImage:[UIImage imageNamed:@"Aku-2.jpg"]];
}

//credits: stackoverflow.com
- (UIViewController *)currentTopViewController
{
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}




@end
