//
//  Jumper.h
//  p03-pradhan
//
//  Created by Nimesh on 2/17/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Jumper : UIImageView
@property (nonatomic) float dx, dy;  // Velocity

@end
