//
//  ViewController.h
//  p03-pradhan
//
//  Created by Nimesh on 2/17/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController


@property (strong, nonatomic) IBOutlet GameView *gameView;
@property (readwrite,weak) UIAlertController *gameOverAlert;


@end

