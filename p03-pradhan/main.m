//
//  main.m
//  p03-pradhan
//
//  Created by Nimesh on 2/18/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
